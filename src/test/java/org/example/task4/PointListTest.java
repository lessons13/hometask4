package org.example.task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointListTest {
    @Test
    public void testAddPoint() {
        Point p1 = new Point(0,0);
        Point p2 = new Point(4,0);
        Point p3 = new Point(0,4);
        Point p4 = new Point(1,1);
        PointList l = new PointList();
        l.addPoint(p1);
        assertEquals(1, l.getNumberOfPoints());
        assertTrue(l.getPoint(0) == p1);
        l.addPoint(p2);
        assertEquals(2, l.getNumberOfPoints());
        assertTrue(l.getPoint(1) == p2);
        l.addPoint(p3);
        assertEquals(3, l.getNumberOfPoints());
        assertTrue(l.getPoint(2) == p3);
        l.addPoint(p4);
        assertEquals(4, l.getNumberOfPoints());
        assertTrue(l.getPoint(3) == p4);
    }

    @Test
    public void testGetNumberOfPoints() {
        PointList list = new PointList();
        Point p1 = new Point(0,0);
        Point p2 = new Point(4,0);

        assertEquals(0, list.getNumberOfPoints());
        list.addPoint(p1);
        assertEquals(1,list.getNumberOfPoints());
        list.addPoint(p2);
        assertEquals(2,list.getNumberOfPoints());
    }

    @Test
    public void testGetPoint() {
        PointList list = new PointList();
        Point p1 = new Point(0,0);
        Point p2 = new Point(4,0);
        list.addPoint(p1);
        list.addPoint(p2);
        assertTrue(p1 == list.getPoint(0));
        assertTrue(p2 == list.getPoint(1));
    }
}