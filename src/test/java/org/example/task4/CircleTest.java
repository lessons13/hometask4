package org.example.task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {
    @Test
    public void testContainsPoint() {
        Point p1 = new Point(0,0);
        Point p2 = new Point(4,0);
        Point p3 = new Point(0,4);
        Point p4 = new Point(1,1);
        Circle r = new Circle(p1, 2);

        assertAll(
                ()-> assertTrue(r.containsPoint(p1)),
                ()-> assertFalse(r.containsPoint(p2)),
                ()-> assertFalse(r.containsPoint(p3)),
                ()-> assertTrue(r.containsPoint(p4))
        );

    }

}