package org.example.task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    @Test
    public void testDistanceTo() {
        Point p1 = new Point(0,0);
        Point p2 = new Point(4,0);
        Point p3 = new Point(0,4);

        assertAll(
                ()-> assertEquals(0,p1.distanceTo(p1)),
                ()-> assertEquals(4,p1.distanceTo(p2)),
                ()-> assertEquals(4,p1.distanceTo(p3))
        );
    }
}