package org.example.task4;

import org.example.task4.menu.Menu;
import org.example.task4.menu.MenuItem;
import org.example.task4.menu.items.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        PointList pointList = new PointList();
        Circle circle = new Circle(new Point(0,0), 1);
        Menu menu = new Menu(scanner, new MenuItem[]{
                new AddPointMenuItem(scanner, pointList),
                new ChangeCenterOfCircleMenuItem(scanner,circle),
                new ChangeRadiusOfCircleMenuItem(scanner,circle),
                new PointsInTheCircleMenuItem(pointList,circle),
                new PointsOutOfTheCircleMenuItem(pointList, circle),
                new ExitMenuItem()
        });
        menu.run();
    }
}