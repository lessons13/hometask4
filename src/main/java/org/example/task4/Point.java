package org.example.task4;

import jdk.jfr.DataAmount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)

public class Point {
    private double x;
    private double y;

    public double distanceTo(Point p) {
        double dx = p.x - x;
        double dy = p.y - y;
        double distanceSquare = dx * dx + dy * dy;
        return Math.sqrt(distanceSquare);
    }

}
