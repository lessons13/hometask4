package org.example.task4;

import java.util.Arrays;

public class PointList {
    private Point[] points = new Point[0];
    private int counter = 0;

    public void addPoint(Point p) {
        if (counter >= points.length) {
            points = Arrays.copyOf(points, points.length + 10);
        }
        points[counter] = p;
        counter++;
    }

    public int getNumberOfPoints() {
        return counter;
    }

    public Point getPoint(int i) {
        return points[i];
    }

}
