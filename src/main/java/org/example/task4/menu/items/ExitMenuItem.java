package org.example.task4.menu.items;

import org.example.task4.menu.MenuItem;

public class ExitMenuItem implements MenuItem {

    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.out.println("Good bye...");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}

