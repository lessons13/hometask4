package org.example.task4.menu.items;

import org.example.task4.Circle;
import org.example.task4.Point;
import org.example.task4.PointList;
import org.example.task4.menu.MenuItem;

public class PointsOutOfTheCircleMenuItem implements MenuItem {
    PointList pointList;
    Circle circle;

    public PointsOutOfTheCircleMenuItem(PointList pointList, Circle circle) {
        this.pointList = pointList;
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Points out of the circle: ";
    }

    @Override
    public void execute() {
        System.out.println("Points out of the circle: ");
        for (int i = 0; i < pointList.getNumberOfPoints(); i++) {
            Point p = pointList.getPoint(i);
            if (!circle.containsPoint(p)) {
                System.out.println(p);
            }
        }
    }
}
