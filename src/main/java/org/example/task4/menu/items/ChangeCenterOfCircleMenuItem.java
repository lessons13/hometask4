package org.example.task4.menu.items;

import org.example.task4.Circle;
import org.example.task4.Point;
import org.example.task4.menu.MenuItem;

import java.util.Scanner;

public class ChangeCenterOfCircleMenuItem implements MenuItem {
    Scanner scanner;
    Circle circle;

    public ChangeCenterOfCircleMenuItem(Scanner scanner, Circle circle) {
        this.scanner = scanner;
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Change center of circle";
    }

    @Override
    public void execute() {
        System.out.print("|Enter center coordinates\n |x: ");
        if (!scanner.hasNextDouble()) {
            System.out.println("Incorrect input");
            scanner.nextLine();
            return;
        }
        double x = scanner.nextDouble();
        System.out.print(" |y: ");
        if (!scanner.hasNextDouble()) {
            System.out.println("Incorrect input");
            scanner.nextLine();
            return;
        }
        double y = scanner.nextDouble();
        Point center = new Point(x, y);
        circle.setCenter(center);
    }
}
