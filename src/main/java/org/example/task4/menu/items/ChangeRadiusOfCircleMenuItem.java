package org.example.task4.menu.items;

import org.example.task4.Circle;
import org.example.task4.menu.MenuItem;

import java.util.Scanner;

public class ChangeRadiusOfCircleMenuItem implements MenuItem {
    Scanner scanner;
    Circle circle;

    public ChangeRadiusOfCircleMenuItem(Scanner scanner, Circle circle) {
        this.scanner = scanner;
        this.circle = circle;
    }

    @Override
    public String getName() {
        return "Change radius of the circle";
    }

    @Override
    public void execute() {
        System.out.print("|Enter radius of the circle: ");
        if (!scanner.hasNextDouble()) {
            System.out.println("Incorrect input");
            scanner.nextLine();
            return;
        }
        double r = scanner.nextDouble();
        circle.setRadius(r);
    }
}
