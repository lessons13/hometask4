package org.example.task4.menu;

public interface MenuItem {
    String getName();
    void execute();

    default boolean isFinal() {
        return false;
    }

}
