package org.example.task4;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class Circle {
    private Point center;
    private double radius;

    public boolean containsPoint(Point p) {
        double distance = center.distanceTo(p);
        return distance <= radius;
    }
}
